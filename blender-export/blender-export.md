In this guide, we will begin with a model in [Blender], and end up with a scene folder that can be imported into Goo.

### Table of contents

* [Install the addon](#part1)
* [Load a file](#part2)
* [Obligatory disclaimer](#part3)

###<a id="part1"></a> Install the addon

Here at Goo, we wouldn't leave you stranded. You want to bring your own models into the engine and use them. We understand that.

For the moment, the easiest way to convert your models into a format usable by Goo, is the [Blender] exporter addon. If you don't know what Blender is, go and find out right away: it's 3D goodness of the free kind (you can design 3D models and animations with it).

You can get the addon in [this page](/download).

Once you have installed and fired up Blender, it's time to install the addon. Start with the default scene (the cube you saw when Blender started). If you have changed anything, just go to the File menu and press New, this should fire up the default scene. The reason is that User settings are saved together with project data.

Next, go to File > User Preferences and click on "Install from File...". Select the .zip file of the addon and click "Install from File..." again. The addon should be installed, or complain if there is an error.

Now go to the User addon list on the left, and enable the addon by clicking on the checkbox.

You're ready to rumble!

###<a id="part2"></a> Load and export a file

For the purposes of this here recipe, we will use a model representing one of the most valuable tools a programmer possesses: a [coffee cup]. Download and extract the model (provided with attribution!), and finally open it in Blender.

You can now export by clicking on File > Export > Goo Engine!

When you do, you are presented with a file chooser, and several options to the left: we suggest the default options.

Choose a name for your scene and a folder to export to. The folder will contain the scene file, as well as several folders with data: `entities/`, `meshes/` etc.

You are now ready to import your scene into Goo! Consider reading our [recipe on the matter](/cookbook/load-scene) to find out how.

###<a id="part3"></a> Obligatory disclaimer

Please note that the exporter is also in alpha, and even though it will definitely work for many simple cases (like, for example -- you guessed it -- cubes), more complex models with advanced effects might not be exported successfully. Fret not, we are on it and will be improving the exporter!

### <a id="part4"></a> Missing/upcoming features and gotchas

You might notice that your exported entities lack materials, textures and so on. Unfortunately, for now you have to add your own materials to the entities, either by editing the actual files, or programmatically. This will be resolved.

Also note that entities need to have a material in Blender, in order to be exported successfully. If you find that you can't find your entities in the exported scene, perhaps you have forgotten to add materials to them in Blender.

Finally, we have to say that your mileage may vary. Simple scenes might be exported successfully, while more complicated ones might have problems. We would appreciate any feedback reports about models that don't work!

[Blender]: http://www.blender.org
[coffee cup]: /system/files/coffee%20cup.zip