### What we've learned so far

In the previous part of this tutorial, we learned the basics of creating a Goo instance and adding it to an HTML page. However, we didn't actually add anything visible to the page. 'Where is all that scrumptious 3D we've been promised?', we hear you cry.

Well, in this part, we will begin adding objects to our Goo instance, and we will actually get to see them in 3D! But first, we must talk about the concept of an [Entity].

### Table of contents

* [Let's add something to look at](#part1)
* [The camera](#part2)
* [The box](#part3)
* [Reference](#ref)

### <a id="part1"></a>Let's add something to look at
In order to put something more interesting on the screen than the default black color we need two things – a camera and something to look at, for example a box. So, how do we add those? We put each one in its own conceptual container called an [Entity].

#### What is an Entity?

An entity is an object in the Goo world. It is usually the representation of a visible object, but it can be other things, like a camera. For something to exist inside the Goo world, it must be represented by an entity.

An entity has many [components][Component], which hold information about what it looks like and what it does. A bare entity (one without any components) is useless and doesn't do anything. Thus, the entity is defined by it components. Components can be of several different types, for example, [transform][TransformComponent], [mesh data][MeshDataComponent] or [camera][CameraComponent] components.

Ok, so… now that we understand what an [Entity] is, let's add the camera and the box, already!

### <a id="part2"></a>The camera

To be able to see anything in the game world, we need a [Camera] to look through. This will be our eyes in the game world.

We first have to create the entity that's going to contain the camera.

**main.js**

    ...
    var cameraEntity = goo.world.createEntity('Camera');
    ...

`goo.world` contains a special [World] entity that keeps track of… well, everything in the game world. The method `goo.world.createEntity('Camera')` creates a *standard* [Entity]. That is, an [Entity] with a [TransformComponent] that keeps track of the [Entity]'s position, rotation and scale. The string argument is simply the name we want to give our new entity, and can be anything you like.

Next, we want to create a [Camera], add that [Camera] to a [CameraComponent], and then attach the [CameraComponent] to `cameraEntity`. Since we're going to use the [Camera] class, we need to include it in the inner `require()` call.

**main.js**

    require(['goo'], function() {
        require([
            'goo/entities/GooRunner',
            'goo/renderer/Camera' // Include the module
        ], function(
            GooRunner,
            Camera // Place the module in this variable
        ) {
    ...

> NOTE: It's important that the included modules are in the same order!

With the [Camera] class included, we can create a new [Camera] instance.

**main.js**

    ...
    var camera = new Camera(35, 1, 0.1, 1000);
    ...

The parameters for the [Camera] constructor are `Camera(fov, aspect, near, far)`.

* `fov` is the full vertical angle of view, in degrees. `35` degrees is good.
* `aspect` is the aspect ratio of the view. Leave this at `1` for most applications.
* `near` is the near plane distance, in Goo Engine units. If a part of some object is closer to the camera than this value it will not be rendered. We'll set this to `0.1`. That's pretty close, aye.
* `far` is the far plane distance, in Goo Engine units. This is the same as `near`,except that it controls how far away something needs to be before the Goo Engine stops rendering it.

The [Camera] won't do us any good unless we put it into a [CameraComponent], so let's add one! A [CameraComponent] is simply a special component that holds a camera.

**main.js**

    ...
    var camera = new Camera(35, 1, 0.1, 1000);
    cameraEntity.setComponent(new CameraComponent(camera));
    ...

We instantiate a [CameraComponent] with its constructor directly as the argument to `cameraEntity.setComponent(component)`. The constructor `CameraComponent(camera)` takes a [Camera] object as its argument.

Finally, let's position `cameraEntity` in and add it to `goo.world`.

**main.js**

    ...
    cameraEntity.transformComponent.transform.translation.set(0, 0, 5);
    cameraEntity.addToWorld();
    ...

This snippet delves a little more into the transform component. A [transform component][TransformComponent] is responsible for an entity's place in 3D space, handling everything from position to direction and size.

The line `cameraEntity.transformComponent.transform.translation.set(0, 0, 5)` goes into `cameraEntity`'s [TransformComponent], then into the [Transform]'s [Translation] object and sets the position with `set(x, y, z)`. In the Goo Engine a position `y` is upwards, and a negative `z` is *into* the screen. By default a [Camera] points in negative z-direction.

Finally, after setting the entity's position, we actually add it to the Goo world. This is the final step to make it visible!

In summary, our code should now look like this.

**main.js**

    ...
    var cameraEntity = goo.world.createEntity('Camera');

    var camera = new Camera(35, 1, 0.1, 1000);
    cameraEntity.setComponent(new CameraComponent(camera));
    cameraEntity.transformComponent.transform.translation.set(0, 0, 5);
    cameraEntity.addToWorld();
    ...

> NOTE: Don't forget to include the CameraComponent from `goo/entities/components/CameraComponent`!

#### What am I looking at?

You might be wondering if we actually added a camera or if we are just trolling you, here. Let us prove our good intentions.

If you run your code now, you might notice how instead of black, you are seeing a gray background. Well, you see, that is the default color that Goo uses unless you instruct it otherwise.

Let's instruct it otherwise so that you'll believe us. Modify your code like this:

    ...
    // Create a new instance of the Goo Engine
    var goo = new GooRunner();
    goo.renderer.setClearColor(1.0, 160/255, 160/255, 1.0);
    ...

You now have a nice pink background.

'Clear color' is the color that Goo uses when it has nothing to display at a particular spot in the world. It is defined as a set of RGBA values between 0 and 1. The first three are simply RGB values, and the fourth one defines opacity. Experiment with other colors if you like.

### <a id="part3"></a>The box

Now that we will be able to see it, it's time to add the box!

A visible entity is different from a camera in the sense that it needs more than just a position. It also requires a shape.

In the 3D world, a shape is called a 'mesh'. Meshes can be anything, from simple shapes to extremely complex space robot models. Here we will use a simple cube. We can use [ShapeCreator], a utility class that contains methods to create meshes for basic shapes.

Also, instead of creating a bare entity with `goo.world.createEntity()`, this time we will use another utility method, `EntityUtils.createTypicalEntity(world, meshData)`, that creates an entity and adds a mesh to it right away.

**main.js**

    ...
    // Create box
    var meshData = ShapeCreator.createBox(1, 1, 1);
    var boxEntity = EntityUtils.createTypicalEntity(goo.world, meshData);
    ...

> NOTE: Include `goo/shapes/ShapeCreator` and `goo/entities/EntityUtils`.

We also need to add the following lines after the previous ones:

**main.js**

    ...
    var material = Material.createMaterial(ShaderLib.texturedLit, 'BoxMaterial');
    boxEntity.meshRendererComponent.materials.push(material);
    ...
> NOTE: Include `goo/renderer/shaders/ShaderLib` and `goo/renderer/Material`.


We won't explain these right away, but we will revisit them in later tutorials that will deal with [materials][Material] and shaders. For now let's just say they will make our box look cool enough to show your friends.

After everything is set, don't forget to add the entity to the world, as well.

**main.js**

    ...
    // Bring the box into the world
    boxEntity.addToWorld();
    ...

As a final touch, you may want to shed some light on the cube.

**main.js**

    ...
    // Shed some light on it
    var light = new PointLight();
    var lightEntity = goo.world.createEntity('light');
    lightEntity.setComponent(new LightComponent(light));

    // Take the light out of the box
    lightEntity.transformComponent.transform.translation.set(0, 3, 3);

    // Add the light to the world
    lightEntity.addToWorld();
    ...

All in all, our code (and its result) should now look like this:


<iframe style="display: block; width: 640px; height: 400px; margin: 0 auto;" src="http://jsfiddle.net/gootechnologies/vYNK9/embedded/result,js/" allowfullscreen="allowfullscreen" frameborder="0"></iframe>


Not impressed, you say? Not a cube, but a square, you say? Well, and we thought there was trust between us by now.

Alright, in the next part you'll *really* see the cube!

In this part of the tutorial, we examined how to add a camera to the world so that things are visible, and we added a simple box entity.

### <a id="ref"></a>Reference
#### Classes we used…
* [GooRunner]
* [Entity]
* [Camera]
* [CameraComponent]
* [ShapeCreator]
* [EntityUtils]
* [ShaderLib]
* [TextureCreator]
* [PointLight]
* [LightComponent]

[GooRunner]: http://gooengine.com/api/GooRunner.html
[Entity]: http://gooengine.com/api/Entity.html
[Camera]: http://gooengine.com/api/Camera.html
[Component]: http://gooengine.com/api/Component.html
[CameraComponent]: http://gooengine.com/api/CameraComponent.html
[TransformComponent]: http://gooengine.com/api/TransformComponent.html
[MeshDataComponent]: http://gooengine.com/api/MeshDataComponent.html
[Transform]: http://gooengine.com/api/Transform.html
[Translation]: http://gooengine.com/api/Translation.html
[ShapeCreator]: http://gooengine.com/api/ShapeCreator.html
[EntityUtils]: http://gooengine.com/api/EntityUtils.html
[ShaderLib]: http://gooengine.com/api/ShaderLib.html
[TextureCreator]: http://gooengine.com/api/TextureCreator.html
[Material]: http://gooengine.com/api/Material.html
[World]: http://gooengine.com/api/World.html
[PointLight]: http://gooengine.com/api/PointLight.html
[LightComponent]: http://gooengine.com/api/LightComponent.html