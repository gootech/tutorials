We will slowly build up to the spinning cube during the course of the next few tutorials. By the time you're done with the current tutorial, you will be able to set up a basic scene, which is the base for any 3D game or application.

### Table of contents

* [Loading the Goo Engine](#part1)
* [Getting the engine to run](#part2)
* [A bit of background](#part3)
* [Reference](#ref)

### <a id="part1"></a>Loading the Goo Engine

In this section we'll get the project workspace set up.

All classes in the Goo Engine are programmed as AMD modules, and are loaded through *Require.js*. What that means in practice is that we will write our application code in its own JavaScript file, and then load it with *Require.js*. So, to start off, let's set up the project workspace.

#### Workspace
`index.html`
*The start page of our application. It contains the base HTML5 markup needed.*

`js/goo.js`
*The Goo Engine code.* [download page](/download)

`js/main.js`
*A JavaScript file containing your application code.*

`js/lib/require.js`
*The module loading library Require.js - [download page](http://requirejs.org/docs/download.html#requirejs).*

`css/style.css`
*A CSS style sheet.*

The project workspace should seem very familiar. The most interesting code will be placed in the `js/main.js` file, but before diving deeper into that, let's have a look at `index.html` and `style.css`.

**index.html**

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>The Basics – Goo Engine Tutorials</title>
        <link rel="stylesheet" href="css/style.css">
        
        <!-- This includes Require.js into the project.
        The `data-main` attribute tells Require.js
        where the game's main script is located. -->
        <script data-main="js/main" src="js/lib/require.js"></script>
    </head>
    <body>
        <!-- page content -->
    </body>
    </html>

Apart from the basic HTML5 structure, there is one line that's especially interesting.

    <script data-main="js/main" src="js/lib/require.js"></script>

The `<script>` tag includes [Require.js], and tells it that our game's main JavaScript file is located in `js/main`. Notice that the `.js` extension of the `main.js` file is not written-out. The reason for that is because [Require.js] wants JavaScript paths without the extension - if your main script would have been in the path `foo/bar/file.js` you would need to write `data-main="foo/bar/file"`.


**style.css**

    canvas {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
    }

The `canvas` style rule will make the `<canvas>` element, which contains our Goo Engine application, fill the whole browser window.

Alright! We're done with our prep work - let's move on to the next section. We're ready to dig into the fun stuff now!

### <a id="part2"></a>Getting the engine to run.

In this section we'll write the base code to get the Goo Engine running. First, let's put some base code into `main.js`.

**main.js**

    require(['goo'], function() {
        require([
            'goo/entities/GooRunner'
        ], function(
            GooRunner
        ) {
            // application code here
        });
    });

The first call to the `require()` function loads the Goo Engine, the second call loads the Goo Engine modules that we want to use in our application. This may look a bit confusing at first, but don't worry, this base code will be the same for all projects.

*We won't go into more detail than necessary about how [Require.js] works, so if you're  curious you are encouraged to take a look at http://requirejs.org/.*

All Goo Engine applications will have at least one thing in common - the [GooRunner]. The [GooRunner] is like a conductor in a symphony - it keeps track of everything needed to keep the Goo Engine running, and it can also tell the Goo Engine to stop running. So, let's add one to `main.js` and then attach it to the DOM.

**main.js**

    ...
    // Create a new instance of the Goo Engine
    var goo = new GooRunner();
    ...

Before we can see anything in the browser, we need to attach the [GooRunner]'s `<canvas>` element to the DOM.

**main.js**

    ...
    // Attach the Goo Engine to the DOM
    // goo.renderer.domElement is a <canvas> element
    document.body.appendChild(goo.renderer.domElement);
    ...

Now when you open up `index.html` in your browser you'll see the whole browser window filled with **a black color**. Below is what we should have on screen!

<iframe style="display: block; width: 640px; height: 400px; margin: 0 auto;" src="http://jsfiddle.net/gootechnologies/BNXBp/embedded/result,js/" allowfullscreen="allowfullscreen" frameborder="0"></iframe>


Above is what your code should look like now (tip: click on 'Javascript'). Black isn't that much fun… so next up we're going to change that into something more interesting.

### <a id="part3"></a> A bit of background

The reason we are only seeing a black background is that to be able to see anything, we must add a [Camera]. However, setting up a Goo Engine instance is enough work for one day, so we will do that in the next tutorial!

You should now be able to set up a workspace for your 3D projects, create a Goo instance and to add it to an HTML page.

### <a id="ref"></a> Reference
#### External
* [Require.js]

#### Classes we used
* [GooRunner]

[Require.js]: http://requirejs.org/
[GooRunner]: http://gooengine.com/api/GooRunner.html
[Camera]: http://gooengine.com/api/Camera.html
