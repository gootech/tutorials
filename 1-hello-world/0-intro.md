# Goo Engine Tutorials

## The Basics

### What we're building

The first part in any tutorial series should be the famous Hello World! example, which is used to show the basics in any programming language. However, printing a little bit of text isn't that much fun, and since we're doing graphics programming it makes more sense to start with the 3D equivalent of Hello World!… the spinning cube!


<iframe style="display: block; width: 640px; height: 400px; margin: 0 auto;" src="http://jsfiddle.net/gootechnologies/EmScv/embedded/result,js/" allowfullscreen="allowfullscreen" frameborder="0"></iframe>