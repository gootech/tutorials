### What we've learned so far

In the previous part of this tutorial, we finally added a camera and a box to our Goo instance.

In this part, we will set the box in motion!

### Table of contents

* [Script this out](#part1)
* [Move it!](#part2)
* [Reference](#ref)

### <a id="part1"></a>Script this out

Before we set the box in motion, there is another Goo concept you need to familiarize yourself with: scripts.

A script is a javascript object containing a `run` function that you can attach to an entity. Scripts can do pretty much anything you like. They can be used to control movement, either automatically or through input, by registering event listeners. They can gather statistics or perform some other function. The `run` function is, well, ran every frame and has access to the entity.

Let's add a script to our box, and then we'll talk a little more about them.

### <a id="part2"></a>Move it!

Scripts are attached to an entity by wrapping them in a [ScriptComponent]. Let's write a simple spinning script for our box. We will use the [World] class to get the global time, in order to make the box spin as time progresses.

Add the following code to your program:

**main.js**


    // Spin the box
    boxEntity.setComponent(new ScriptComponent({
        run: function (entity) {
            entity.transformComponent.transform.setRotationXYZ(
                World.time * 1.2,
                World.time * 2.0,
                0
            );
            entity.transformComponent.setUpdated();
        }
    }));
> NOTE: Include `goo/entities/components/ScriptComponent` and `goo/entities/World`.



Let's examine this code from the inside out.

    {
        run: function (entity) {
            entity.transformComponent.transform.setRotationXYZ(
                World.time * 1.2,
                World.time * 2.0,
                0
            );
            entity.transformComponent.setUpdated();
       }
    }


At the heart lies our script: an object with a `run` function. The function is run every frame, and it uses `World.time`, the global time, to update the entity's rotation in each axis. The arguments represent rotation around the respective axis, in radians.

After each update, the transform component of the entity is set to 'updated' status, so that Goo knows that something has changed and it should be processed.

On the next level, this object is given as an argument to a [ScriptComponent]. A ScriptComponent can have multiple scripts, and each is run every frame, in turn. A ScriptComponent initialized with a script just contains that one script for now. If we want to later add more scripts, we could just push one to the ScriptComponent's `scripts` property, which is an Array.

Finally, the ScriptComponent is attached to the object. The scripts contained in it are now active.

The outcome is, finally, very obviously a spinning cube!


<iframe style="display: block; width: 640px; height: 400px; margin: 0 auto;" src="http://jsfiddle.net/gootechnologies/EmScv/embedded/result,js/" allowfullscreen="allowfullscreen" frameborder="0"></iframe>


Congratulations! You have built  a 3D 'Hello world!' in the Goo Engine! You are now ready to delve deeper in the Goo world!

### <a id="ref"></a>Reference

#### Classes we used

* [ScriptComponent]
* [World]

[ScriptComponent]: http://gooengine.com/api/ScriptComponent.html
[World]: http://gooengine.com/api/World.html
