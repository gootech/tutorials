import re
import sys
import os

input_file_name = sys.argv[1]
output_file_name = os.path.splitext(input_file_name)[0]+".toc.md"

header_pattern = re.compile(r'^(?P<depth>##+)\s(?P<section_title>.*)\s*$')
toc_template = "%s* [%s](#%s)\n"
header_template = '%s<a id="%s"></a> %s\n'
part_counter = 1
toc = ["### Table of contents\n"]
toc_position = 0  # if TOC placeholder not found, place it in the beginning

with open(input_file_name) as input_file:
    lines = list(input_file)
    for line_number, line in enumerate(lines):
        match = header_pattern.match(line)
        if match:
            # sanity check: if the file already has a TOC, warn the user
            if match.group('section_title') == "Table of contents":
                raise Exception('You already have a Table of contents: please delete and rerun')
                
            if match.group('section_title') == "Reference":
                anchor = "ref"
            else:
                anchor = "part%d" % part_counter
            
            print "'%s' matched" % line.rstrip('\n')
            depth = len(match.group('depth')) - 3  # indent according to depth
            
            toc_line = toc_template % (" " * (depth*2), match.group('section_title'), anchor)  # add a toc entry for this section
            lines[line_number] = header_template % ( match.group('depth'), anchor, match.group('section_title') )  # replace line to add anchor tag
            
            print "appending %s" % toc_line
            toc.append(toc_line)
            part_counter += 1
        elif line.strip() == "[TOC]":
            toc_position = line_number

lines[toc_position:toc_position+1] = toc  # replace TOC placeholder with actual TOC

with open(output_file_name, 'wb') as output_file:
    output_file.write("".join(lines))