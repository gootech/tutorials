require.config({
	//baseUrl : "../../goojs",
	paths : {
		goo : "../../goojs/src/goo",
		'goo/lib': '../../lib'
	}
});
require([
	'goo/entities/components/MeshDataComponent',
	'goo/entities/components/MeshRendererComponent',
	'goo/renderer/Material',
	'goo/entities/GooRunner',
	'goo/renderer/TextureCreator',
	'goo/entities/components/ScriptComponent',
	'goo/shapes/ShapeCreator',
	'goo/entities/EntityUtils',
	'goo/renderer/Texture',
	'goo/renderer/Camera',
	'goo/entities/components/CameraComponent',
	'goo/math/Vector3',
	'goo/scripts/WASDControlScript',
	'goo/scripts/MouseLookControlScript',
	'goo/entities/systems/ParticlesSystem',
	'goo/entities/components/ParticleComponent',
	'goo/particles/ParticleUtils',
	'goo/renderer/shaders/ShaderLib'
], function (
	MeshDataComponent,
	MeshRendererComponent,
	Material,
	GooRunner,
	TextureCreator,
	ScriptComponent,
	ShapeCreator,
	EntityUtils,
	Texture,
	Camera,
	CameraComponent,
	Vector3,
	WASDControlScript,
	MouseLookControlScript,
	ParticlesSystem,
	ParticleComponent,
	ParticleUtils,
	ShaderLib) {
	"use strict";

	var resourcePath = "";

	var material;

	function init () {
		// Create typical goo application
		var goo = new GooRunner({
			showStats : true
		});
		goo.renderer.domElement.id = 'goo';
		document.body.appendChild(goo.renderer.domElement);

		// Create particle material
		material = Material.createMaterial(ShaderLib.particles);
		var texture = new TextureCreator().loadTexture2D(resourcePath + '/flare.png');
		texture.wrapS = 'EdgeClamp';
		texture.wrapT = 'EdgeClamp';
		texture.generateMipmaps = true;
		material.textures.push(texture);
		material.blendState.blending = 'AdditiveBlending';
		material.cullState.enabled = false;
		material.depthState.write = false;

		// Add ParticlesSystem to world
		var particles = new ParticlesSystem();
		goo.world.setSystem(particles);

		// Create an entity with particles
		createParticles(goo);

		// Add camera
		var camera = new Camera(45, 1, 1, 1000);
		var cameraEntity = goo.world.createEntity("CameraEntity");
		cameraEntity.transformComponent.transform.translation.set(0, 0, 20);
		cameraEntity.transformComponent.transform.lookAt(new Vector3(0, 0, 0), Vector3.UNIT_Y);
		cameraEntity.setComponent(new CameraComponent(camera));
		cameraEntity.addToWorld();

		var scripts = new ScriptComponent();
		scripts.scripts.push(new WASDControlScript({
			domElement : goo.renderer.domElement,
			walkSpeed : 25.0,
			crawlSpeed : 10.0
		}));
		scripts.scripts.push(new MouseLookControlScript({
			domElement : goo.renderer.domElement
		}));
		cameraEntity.setComponent(scripts);

		function createParticles (goo) {
			// Create the particle cloud entity
			var entity = goo.world.createEntity();

			// Create particle component of the particle cloud entity
			var particleComponent = new ParticleComponent({
				timeline : [{
					timeOffset : 0.0,
					spin : 0,
					mass : 1,
					size : 2.0,
					color : [1, 1, 0, 0.5]
				}, {
					timeOffset : 0.25,
					color : [1, 0, 0, 1]
				}, {
					timeOffset : 0.25,
					color : [0, 0, 0, 0.7]
				}, {
					timeOffset : 0.5,
					size : 3.0,
					color : [0, 0, 0, 0]
				}],
				emitters : [{
					totalParticlesToSpawn : -1,
					releaseRatePerSecond : 15,
					minLifetime : 1.0,
					maxLifetime : 2.5,
					getEmissionPoint : function (particle, particleEntity) {
						var vec3 = particle.position;
						return ParticleUtils.applyEntityTransformPoint(vec3.set(0, 0, 0), particleEntity);
					},
					getEmissionVelocity : function (particle, particleEntity) {
						var vec3 = particle.velocity;
						return ParticleUtils.getRandomVelocityOffY(vec3, 0, Math.PI * 15 / 180, 5);
					}
				}]
			});
			entity.setComponent(particleComponent);

			// Create meshData component using particle data
			var meshDataComponent = new MeshDataComponent(particleComponent.meshData);
			entity.setComponent(meshDataComponent);

			// Create meshRenderer component with material and shader
			var meshRendererComponent = new MeshRendererComponent();
			meshRendererComponent.materials.push(material);
			entity.setComponent(meshRendererComponent);

			entity.addToWorld();
		}
	}

	init();
});
