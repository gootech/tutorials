/*
Demonstrates how to load a scene from config files.

The scene config (simple.scene) contains references to all entities
that should be loaded.
*/

require([
	'goo/loaders/Loader',
	'goo/loaders/SceneLoader',
	'goo/entities/GooRunner'
], function (
	Loader,
	SceneLoader,
	GooRunner
) {
	'use strict';

	function init() {
		// Create typical goo application
		var goo = new GooRunner();
		goo.renderer.domElement.id = 'goo';
		document.body.appendChild(goo.renderer.domElement);
	}

	init();
});
