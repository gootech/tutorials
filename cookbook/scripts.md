In this recipe we will learn about Goo's built-in scripts, which enable you to get things going in the Goo world. Scripts are added to a [ScriptComponent], like this

	var scriptComponent = new ScriptComponent();

	// `scriptComponent.scripts` is an array of scripts.
	// We use the Array.push() method to add a script to the
	// ScriptComponent.
	scriptComponent.scripts.push(new OrbitCamControlScript({
		domElement: goo.renderer.domElement,
		spherical: new Vector3(20, 0, Math.PI/6),
		lookAtPoint: new Vector3(2, 0, 0),
		minAscent: 0,
		maxZoomDistance: 25
	}));

	// Remember to attach the ScriptComponent to your entity!
	myEntity.setComponent(scriptComponent);

# OrbitCamControlScript

This script is usually added to a camera, instructed to look at a specific spot, and Mona-Lisa the *hell* out of it: no matter how you rotate it, it will keep looking at that spot.

This is cool when you want to look an object from all sides.

## Code

<iframe style="display: block; width: 640px; height: 400px; margin: 0 auto;" src="http://jsfiddle.net/gootechnologies/Gks97/embedded/result,js/" allowfullscreen="allowfullscreen" frameborder="0"></iframe>

The most important things in the life of a camera are where it *is* and where it's *looking at*. Pretty much all there is in a camera's life, really.

These are controlled by the `spherical` and `lookAtPoint` properties of the initialization object.

Quite obviously, the initial position is given in [spherical coordinates](http://en.wikipedia.org/wiki/Spherical_coordinate_system). The look-at point is just a plain ol' point in 3D space.

Now it's your turn to play around with it. Don't forget to check the [API][OrbitCamControlScript] for more options!

# WASDControlScript

This script enables you to move around using the W, A, S and D keys. Attach it to a camera or a character, and explore! Just be careful not to get [Duke Nukem's disease](http://www.youtube.com/watch?v=-jBKKV2V8eU).

## Code

<iframe style="display: block; width: 640px; height: 400px; margin: 0 auto;" src="http://jsfiddle.net/gootechnologies/xtX7z/embedded/result,js/" allowfullscreen="allowfullscreen" frameborder="0"></iframe>

You could even try attaching it to the box instead of the camera. Try it!

# Simple Camera Follow script

It is possible to write your own scripts if none of the ones included in the engine suits your needs.
Just to present the possibilities that scripts can give, here's a script that makes a camera follow an entity around, and makes it appear like it's floating.

## Code

<iframe style="display: block; width: 640px; height: 400px; margin: 0 auto;" src="http://jsfiddle.net/gootechnologies/NSq2w/embedded/result,js/" allowfullscreen="allowfullscreen" frameborder="0"></iframe>

# Reference

* [ScriptComponent]
* [OrbitCamControlScript]
* [WASDControlScript]

[OrbitCamControlScript]: /api/OrbitCamControlScript.html
[WASDControlScript]: /api/WASDControlScript.html
[ScriptComponent]: /api/ScriptComponent.html
