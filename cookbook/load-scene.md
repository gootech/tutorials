In this recipe, we will learn how to use the loading system of Goo to load an entire scene.

## Code

		// The DynamicLoader takes care of loading data from a URL.
        var loader = new DynamicLoader({
            world: goo.world,
            rootPath: './scene/'
        });

        loader.load('simple.scene').then(function(entities) {
            // This function is called when all the entities
            // and their dependencies have been loaded.
            console.log('Success!');
        }).then(null, function(e) {
            // The second parameter of `then` is an error handling function.
            // We just pop up an error message in case the scene fails to load.
            alert('Failed to load scene: ' + e);
        });

## Explanation
### Table of contents

* [What is a scene?](#part1)
  * [Our sample scene](#part2)
* [Don't make a sync, darling](#part3)
* [Use the scene, Luke](#part4)
* [Beyond the pen](#part5)
* [Reference](#ref)

###<a id="part1"></a> What is a scene?

A *scene* is a collection of models arranged in a specific way, and given specific properties and so on. It is not unlike a scene in a movie production or a level in a game. Scenes can be saved into files, so that we don't need to create them in pure code. Often enough, scenes will be created in specialized programs that makes the process easier than coding (examples include Blender, Maya and Sketchup).

Goo has its own [file format][file-format] to describe scenes, which, in brief, is a collection of JSON files describing the entities comprising the scene. Let us not concern ourselves with how one would obtain such a scene (but if one was so disposed, they would have to [know where to look][blender-export]), let us just say we've got a scene already. We will examine how to actually load it into the Engine to actually use in a game, a project or for world domination.

####<a id="part2"></a> Our sample scene

For those poor souls who lack a scene, we will provide one for the sake of this recipe. It's, once again, the infamous simple cube. The point here is to learn, not to create impressive visuals. At least for the time being. Patience, young padawan.

> NOTE Of course, if you came from [that other tutorial][blender-export], you might have a different scene to load.

As we will need to load files from the server, we cannot use our (and presumably your) beloved [jsFiddle] for this code. However, that gives you the chance to get your hands a little dirty.

Grab the recipe basic code from [here][downloadable] and extract it somewhere on your computer. Examine the contents of the `scene/` folder: that is where the scene data is contained. The file `simple.scene` is the entry point to load the scene.

You now need to run a basic server to serve the folder. If you have python, we recommend:

    python -m SimpleHTTPServer  # python 2.x
    python -m http.server  # python 3.x

That should make the example accessible at [http://localhost:8000](http://localhost:8000). Try it.

You should get a black screen, because the code does nothing but create a Goo instance.There wouldn't be much sense in just giving you the code, would there? You'll have to read on.

###<a id="part3"></a> Don't make a sync, darling

One thing that needs to be noted is that in the Goo Engine, stuff is loaded asynchronously. We cannot just block and wait for everything to load, that would be a waste of time. What we do instead is use [Promises][Promise]. The flavour of Promises Goo uses is [rsvp.js][RSVP].

You needn't be an expert in Promises to load something in the Goo Engine, this page provides examples of the things you will need most of the time. However, it might be a good idea to take a look at the `rsvp.js` page to have a better grasp of them. So that you don't shoot yourself in the foot, feet are important.

The basic thing you need to know is that a Promise supports `.then()` calls:

    // promise returned by a Goo loader
    promise.then(function(value) {
      // success
    }, function(value) {
      // failure
    });

Let's see how we can use the above to load a simple scene.

###<a id="part4"></a> Use the scene, Luke!

First, let's create a dynamic loader:

**loading.js**

    var goo = new GooRunner();
    goo.renderer.domElement.id = 'goo';
    document.body.appendChild(goo.renderer.domElement);

    // The DynamicLoader takes care of loading data from a URL.
    var loader = new DynamicLoader({
         world: goo.world,
         rootPath: './scene/'
    });

Now, we can tell it to load out scene:

**loading.js**

    loader.load('simple.scene').then(function(entities) {
         // This function is called when all the entities
         // and their dependencies have been loaded.
         console.log('Success!');
    }).then(null, function(e) {
         // The second parameter of `then` is an error handling function.
         // We just pop up an error message in case the scene fails to load.
         alert('Failed to load scene: ' + e);
    });

`loader.load('simple.scene')` schedules the loading of `simple.scene` (which lies under `./scene`) and returns a Promise. We then attach a callback to that Promise with the, well, `.then()` call.

Notice how we chain a second `.then()` call after the first one, with a `null` first argument and an error handling function (a very ugly one, do not try this at /home!). Any error encountered in the previous callbacks will end up in that function.

Reload the page on your browser, and you will now see our all-too-familiar cube!

###<a id="part5"></a> Beyond the pen

Note that in the code above, there is no mention of the contents of the scene: whatever is contained in the files gets loaded. That means that you could load your own scenes in much the same way. Simply replace the `scene` folder with another. Check the [Blender exporter][blender-export] guide to create your own scene!

###<a id="ref"></a> Reference

* [GooRunner]
* [DynamicLoader]

[blender-export]: /export-scene-blender
[file-format]: /goo-file-format
[downloadable]: http://gooengine.com/system/files/loading.zip
[jsFiddle]: http://jsfiddle.net
[Promise]: http://promises-aplus.github.io/promises-spec/
[RSVP]: https://github.com/tildeio/rsvp.js
[Loader]: http://gooengine.com/api/Loader.html
[BundleLoader]: http://gooengine.com/api/BundleLoader.html
[SceneLoader]: http://gooengine.com/api/SceneLoader.html
[MeshLoader]: http://gooengine.com/api/MeshLoader.html
[EntityLoader]: http://gooengine.com/api/EntityLoader.html
[GooRunner]: http://gooengine.com/api/GooRunner.html