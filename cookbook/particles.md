In this recipe we will learn how to put particles on the screen.

# Setup
In order for us to see any particles whatsoever, we first need to do a little bit of setup.
First off, we have the standard goo world creation recipe:

    var goo = new GooRunner();
    goo.renderer.domElement.id = 'goo';
    document.body.appendChild(goo.renderer.domElement);

Next, we will need some particle material. There are 2 essential components to bear in mind here: the `particles` shader
and the texture.

    // Particle material
    material = Material.createMaterial(ShaderLib.particles);
    var texture = new TextureCreator().loadTexture2D(resourcePath + '/flare.png');
    texture.wrapS = 'EdgeClamp';
    texture.wrapT = 'EdgeClamp';
    texture.generateMipmaps = true;
    material.textures.push(texture);
    material.blendState.blending = 'AlphaBlending';
    material.cullState.enabled = false;
    material.depthState.write = false;

Ultimately you can skip the texture - in case you want your particles to have a blocky feel - but for the purpose of this tutorial we'll consider a nice flare-y texture because we'll want to light a fire after all.
Another two factors that you want to keep in mind are that you usually want some form of blending (and textures with an alpha channel) and to have mipmaps generated beforehand to speed up the rendering process.

The last step in the setup phase is telling the Goo World how to handle particles. This is where the particle system fits in:

    // Add a particle system to world.
    var particles = new ParticlesSystem();
    goo.world.setSystem(particles);

# Let's start a fire!

Now that we have all the materials, all we need is to light them up.
Don't forget that everything in the Goo Universe is an entity. Even particles need to be represented by an entity.

    // Create the particle cloud entity
    var entity = goo.world.createEntity();

    // Create particle component of the particle cloud entity
    var particleComponent = new ParticleComponent({
        timeline : [{
            timeOffset : 0.0,
            spin : 0,
            mass : 1,
            size : 2.0,
            color : [1, 1, 0, 0.5]
        }, {
            timeOffset : 0.25,
            color : [1, 0, 0, 1]
        }, {
            timeOffset : 0.25,
            color : [0, 0, 0, 0.7]
        }, {
            timeOffset : 0.5,
            size : 3.0,
            color : [0, 0, 0, 0]
        }],
        emitters : [{
            totalParticlesToSpawn : -1,
            releaseRatePerSecond : 5,
            minLifetime : 1.0,
            maxLifetime : 2.5,
            getEmissionPoint : function (particle, particleEntity) {
                var vec3 = particle.position;
                return ParticleUtils.applyEntityTransformPoint(vec3.set(0, 0, 0), particleEntity);
            },
            getEmissionVelocity : function (particle, particleEntity) {
                var vec3 = particle.velocity;
                return ParticleUtils.getRandomVelocityOffY(vec3, 0, Math.PI * 15 / 180, 5);
            }
        }],
    });
    entity.setComponent(particleComponent);

    // Create meshData component using particle data
    var meshDataComponent = new MeshDataComponent(particleComponent.meshData);
    entity.setComponent(meshDataComponent);

    // Create meshRenderer component with material and shader
    var meshRendererComponent = new MeshRendererComponent();
    meshRendererComponent.materials.push(material);
    entity.setComponent(meshRendererComponent);

    entity.addToWorld();

The most interesting parameters to feast you eyes on are the ones you feed the particle component with. Here is where you define the behaviour of the particles.

First, there's the `timeline` array which defines how a particle should evolve as it gets older.
You can see that at the moment a particle is born (the first entry has `timeOffset` equal to 0.0), it is given its spin (whether it spins or not), its mass, size and color. After exactly 0.25 seconds (notice the second entry's `timeOffset`) the particle needs to transition to a different size and color and so on until it dies.

Second, there's the emitter array. A particle component may have more than one emitter, but our little camp fire has only one single fire source.

The first parameter we've used (`totalParticlesToSpawn`) sets exactly how many particles we want this emitter to spawn. If it's -1 then it's going to keep spawning particles forever.

The second parameter (`releaseRatePerSecond`) tells the particle emitter how many particles it should spawn every second. 5 is a good value for our fireplace (we're not burning rocket fuel). If you're dealing with situations like exploding fireworks then you'll probably want to crank this number up to 100 and have the `totalParticlesToSpawn` also set to 100 as you'll want a single big release of magical glittery particles.

The next two parameters (`minLifetime` and `maxLifetime`) give the expected age range of the particles this emitter is going to spawn.

The second-to-last parameter we're using for our fire place is `getEmissionPoint` which is a function and not a value. As it is in the code it does absolutely nothing, but I've left it there so that you can get a feel of how configurable an emitter is. Replace the last statement of that function with

    var randomAngle = Math.random() * Math.PI * 2;
    return ParticleUtils.applyEntityTransformPoint(
        vec3.set(Math.cos(randomAngle) * 5, 0, Math.sin(randomAngle) * 5), particleEntity);

and increase the `releaseRatePerSecond` to about 50. Voilà, you now have yourself a fire-circle!

And finally, the last parameter and the one that gives a more lively feel is `getEmissionVelocity` which is also a function and is responsible for assigning newly born particles a speed.

There, the fire is up and running (burning).

# Don't forget the camera

Of course, we still have the camera to add and, as a little extra, I've set up a camera control script. Now you'll be able to control the view by dragging the mouse or hitting the WASD to walk round the campfire.

    // Add camera
    var camera = new Camera(45, 1, 1, 1000);
    var cameraEntity = goo.world.createEntity("CameraEntity");
    cameraEntity.transformComponent.transform.translation.set(0, 0, 20);
    cameraEntity.transformComponent.transform.lookAt(new Vector3(0, 0, 0), Vector3.UNIT_Y);
    cameraEntity.setComponent(new CameraComponent(camera));
    cameraEntity.addToWorld();

    // Camera control set up
    var scripts = new ScriptComponent();
    scripts.scripts.push(new WASDControlScript({
        domElement : goo.renderer.domElement,
        walkSpeed : 25.0,
        crawlSpeed : 10.0
    }));
    scripts.scripts.push(new MouseLookControlScript({
        domElement : goo.renderer.domElement
    }));
    cameraEntity.setComponent(scripts);

# Orientation, orientation, orientation

Particles need not always look the same from every angle.
Take for example the tiny water ripples that form in pools when it rains - they are aligned to the surface of the water and not always facing the camera.

The way to change the default always-face-the-camera orientation is to add the `getParticleBillboardVectors` property to the emitter.

Here's an example:

    getParticleBillboardVectors : function (particle, particleEntity) {
        particle.bbX.set(-1, 0, 0);
        particle.bbY.set(0, 0, -1);
    }

To get nice watter ripples you'll also need to have a water ripple texture (concentric circles), the particles staying still, growing in size while they fade and using `AlphaBlending` instead of `AdditiveBlending`. This is left as an exercise to the reader.

# Reference

* [ParticlesSystem]
* [ParticleComponent]
* [ParticleUtils]
* [ScriptComponent]
* [MouseLookControlScript]
* [WASDControlScript]

[ParticlesSystem]: /api/ParticlesSystem.html
[ParticleComponent]: /api/ParticleComponent.html
[ParticleUtils]: /api/ParticleUtils.html
[MouseLookControlScript]: /api/MouseLookControlScript.html
[WASDControlScript]: /api/WASDControlScript.html
[ScriptComponent]: /api/ScriptComponent.html
