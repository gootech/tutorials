When using the Goo Engine, you probably want to display a bunch of neat objects, lit in a certain way, displayed from a certain angle. Instead of creating this setup programmatically, we have designed a way to load it with JSON-formatted configuration objects, or **configs**. The configs represent different parts in the engine, so there are entity-configs, material-configs, shader-configs, etc. In this page we'll take a look at the various config types.

To identify configs, we use **references** (*refs*). So the ref `entities/boy.entity` would perhaps reference a config representing a model of a boy.

Configs are usually kept in separate files, with their file path as their ref. So config `entities/boy.entity` would be a JSON-formatted text file called `boy.entity`, residing in the folder `entities`.

As you might have guessed, files have extensions that show what they are, like `.entity`, `.scene` or `.material`. They are, by convention, stored in separate folders as well, and then collected into *scenes*.

### Table of contents
* [Scene config](#part1)
* [Entity config](#part2)
* [Mesh config](#part3)
* [Material config](#part4)
* [Texture config](#part5)
* [Shader config](#part6)
* [Bundles](#part7)
* [Creating a scene](#part8)
* [Reference](#ref)

###<a id="part1"></a> Scene config
To gather all the neat stuff you want to put in your 3D-environment, we have designed the **scene**-config. The scene-config holds references to all the entities that will be loaded into the engine, in the array `entityRefs`.

    {
        "entityRefs": [
            "entities/an.entity",
            "entities/another.entity"
            "entities/seehowmuchfunwerehavingwithextensionsquestionmark.entity"
        ]
    }

###<a id="part2"></a> Entity config
The entity-config holds data about how the different components will be set up. For more information about components, check [the API][API].

Notably, almost all entity-configs will have settings for the *transform* component, i.e. scale, rotation and translation along the X-, Y- and Z-axis. The rotation is in degrees, describing the rotation around respective axis.

Don't be fooled, these are all fancy names to describe that with the transform component, you control *where* you want your entity, *how big* you want it and *where it should face*.

Sometimes, the transform also has a `parentRef` which the entity's transform is relative to. For example, a hand is linked to the arm, so if you rotate the arm, the hand will move. This is done by setting `entities/arm.entity` as `parentRef` of `entities/hand.entity`.

Other components to watch out for is `meshData` and `meshRenderer` which hold settings for their respective components. `meshData` holds a ref to the mesh-config in `meshRef` (notice how all keys referencing another config end with Ref?). `materialRefs` is an array of refs to materials, since a renderable entity can have several materials to blend to get the final result.

*Example entity config*

    {
        "components": {
            "meshData": {
                "meshRef": "meshes/another.mesh"
            },
            "meshRenderer": {
                "castShadows": false,
                "cullMode": "Dynamic",
                "materialRefs": [
                    "materials/metal.material"
                ],
                "receiveShadows": false
            },
            "transform": {
                "rotation": [
                    0,
                    0,
                    0
                ],
                "scale": [
                    1,
                    1,
                    1
                ],
                "translation": [
                    0,
                    0,
                    0
                ],
                "parentRef": "entities/an.entity"
            }
        },
        "name": "Another geometry",
    }

###<a id="part3"></a> Mesh config
The mesh config contains all the vertices and faces, which can be pretty big, complicated and hard to edit manually. Don't disturb it, it might bite you. Just know that it's there.

###<a id="part4"></a> Material config
Material configs of course contain information about how to setup materials. You can set *uniforms* such as the colors for `materialAmbient`, `materialDiffuse`, `materialEmissive` and `materialSpecular`. With a trained eye, you can also spot the array of `textureRefs` listing refs to texture-configs. There is also the `shaderRef`, defining what shader to use for the material.

*Example material config*

    {
        "name": "Metal",
        "textureRefs": [
            "textures/metal.texture"
        ],
        "uniforms": {
            "materialAmbient": [
                0.6,
                0.6,
                0.6,
                1
            ],
            "materialDiffuse": [
                0.5,
                0.5,
                0.5,
                1
            ],
            "materialEmissive": [
                0,
                0,
                0,
                1
            ],
            "materialSpecular": [
                0,
                0,
                0,
                1
            ],
            "materialSpecularPower": 1,
            "opacity": 1
        },
        shaderRef": "shaders/texturedLit.shader"
    }

###<a id="part5"></a> Texture config
The interesting part about the texture-config is the `url` property, where the actual image is stored. Although config objects can be used as separate files or bundled json, images are binary and the url is the path to the image relative to the scene file path.

*Example texture config*

    {
        "url": "textures/teapot.png"
    }

###<a id="part6"></a> Shader config
You will usually use shaders defined by someone else, so there will be no need to edit attributes and uniforms manually. They are responsible for connecting GLSL-shader variables to in-engine values. `vshaderRef` and `fshaderRef` are, as you might have figured out already, refs to other configs, namely the vertex shader and the fragment shader. These configs are plain text GLSL code. You can look [here][GLSL] if you want to learn more on how to write shaders for WebGL.

*Example shader config*

    {
        "attributes": {
            "vertexPosition": "POSITION",
            "vertexNormal": "NORMAL",
            "vertexUV0": "TEXCOORD0"
        },
        "uniforms": {
            "viewMatrix": "VIEW_MATRIX",
            "projectionMatrix": "PROJECTION_MATRIX",
            "worldMatrix": "WORLD_MATRIX",
            "cameraPosition": "CAMERA",
            "lightPosition": "LIGHT0",
            "diffuseMap": "TEXTURE0",
            "materialAmbient": "AMBIENT",
            "materialDiffuse": "DIFFUSE",
            "materialSpecular": "SPECULAR",
            "materialSpecularPower": "SPECULAR_POWER"
        },
        "vshaderRef": "shaders/texturedLit.vert",
        "fshaderRef": "shaders/texturedLit.frag"
    }

###<a id="part7"></a> Bundles
Usually, a scene is stored in a directory structure like that:

    entities/
    materials/
    shaders/
    textures/
    bliergh.scene

However, you might benefit by just shoving all configs into a single, large file, called a *bundle*. Bundles are also JSON-formatted and simply contain an object with all the stuff contained in a scene, indexed by their ref. This might have sounded complicated, so just take a look:

*Example bundle file*

    {
        'entity/supreme.entity': {
            ...
        },
        ...
        'material/indestructible.material': {
        },
        ...
        'bliergh.scene': {
            'entityRefs': ['entity/supreme.entity', ...]
        }
    }

###<a id="part8"></a> Creating a scene
After reading this we hope you see the use of having your scene in a nice on-the-side file, but writing these files yourself sounds tedious. Good news! Pretty soon we plan to release a converter that converts project files from various 3D file types into our own! Until then, there is also a [blender plugin][blender-export] you can fiddle with.

So go on - create epic content and load it into the engine! If you want to have a closer look on how a sample project can be laid out, or how you can load it, check out the [load-a-scene recipe][load-scene].


###<a id="ref"></a> Reference

* [API]

[API]: /api
[MeshData]: /api/MeshDataComponent.html
[MeshRenderer]: /api/MeshRendererComponent.html
[Camera]: /api/Camera.html
[Material]: /api/Material.html
[GLSL]: http://www.opengl.org/wiki/GLSL
[blender-export]: /export-scene-blender
[load-scene]: /load-scene